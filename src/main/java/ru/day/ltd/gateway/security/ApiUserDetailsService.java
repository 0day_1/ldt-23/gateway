package ru.day.ltd.gateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.day.ltd.gateway.data.entity.ApiUser;
import ru.day.ltd.gateway.data.repo.ApiUserRepository;

import java.util.Optional;

@Service(ApiUserDetailsService.NAME)
public class ApiUserDetailsService implements UserDetailsService {

    public static final String NAME = "apiUserDetailsService";

    private final ApiUserRepository apiUserRepository;

    @Autowired
    public ApiUserDetailsService(ApiUserRepository apiUserRepository) {
        this.apiUserRepository = apiUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ApiUser> apiUser = apiUserRepository.findByUsername(username);

        if (apiUser.isEmpty())
            throw new UsernameNotFoundException("User not found");

        return new ApiUserDetails(apiUser.get());
    }
}

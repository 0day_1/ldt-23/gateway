package ru.day.ltd.gateway.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.day.ltd.gateway.data.enums.UserType;
import ru.day.ltd.gateway.service.ApiUserService;

import java.util.Collections;
import java.util.Objects;

@Slf4j
@Component
public class AdminUserInitializer implements ApplicationRunner {

    private final Environment env;
    private final ApiUserService apiUserService;

    public AdminUserInitializer(Environment env, ApiUserService apiUserService) {
        this.env = env;
        this.apiUserService = apiUserService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // set up security context with admin role
        SecurityContext emptyContext = SecurityContextHolder.createEmptyContext();
        Authentication auth = new UsernamePasswordAuthenticationToken("system",
                "system",
                Collections.singleton(new SimpleGrantedAuthority(UserType.ADMIN.getAuthorityString())));
        SecurityContextHolder.getContext().setAuthentication(auth);
        if (apiUserService.getAll().isEmpty()) {
            apiUserService.register(Objects.requireNonNull(env.getProperty("ru.day.ltd.gateway.admin.login")),
                    Objects.requireNonNull(env.getProperty("ru.day.ltd.gateway.admin.password")),
                    UserType.ADMIN);
        }
        SecurityContextHolder.clearContext();
    }
}

package ru.day.ltd.gateway.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.mvc.ProxyExchange;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.day.ltd.gateway.data.entity.Microservice;
import ru.day.ltd.gateway.service.MicroserviceService;

import javax.servlet.http.HttpServletRequest;

// FIXME bad practice
@Slf4j
@RestController
@RequestMapping("/")
public class GatewayController {

    private final MicroserviceService microserviceService;

    public GatewayController(MicroserviceService microserviceService) {
        this.microserviceService = microserviceService;
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE},
            value = "/{pathParam}/**")
    public ResponseEntity<?> proxyPath(ProxyExchange<byte[]> proxy,
                                       HttpServletRequest request,
                                       @PathVariable String pathParam) {

        if (pathParam == null) {
            return ResponseEntity.badRequest().body("Path param is null");
        }
        Microservice microservice;
        try {
            microservice = microserviceService.getByPath(pathParam);
        } catch (IllegalArgumentException iae) {
            log.error("Can't get microservice", iae);
            return ResponseEntity.badRequest().body(iae.getMessage());
        }
        String path = proxy.path("/" + pathParam);
        String redirectUrl = getRedirectUrl(microservice, path);
        proxy.uri(redirectUrl);
        switch (request.getMethod()) {
            case "GET":
                log.info("redirect GET to {}", redirectUrl);
                return proxy.get();
            case "POST":
                log.info("redirect POST to {}", redirectUrl);
                return proxy.post();
            case "PUT":
                log.info("redirect PUT to {}", redirectUrl);
                return proxy.put();
            case "DELETE":
                log.info("redirect DELETE to {}", redirectUrl);
                return proxy.delete();
            default:
                log.warn("no redirect {}", request.getMethod());
                return ResponseEntity.badRequest().body("Unsupported method");
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/register-new-microservice")
    public ResponseEntity<?> registerNewMicroservice(@RequestParam String name,
                                                     @RequestParam String url,
                                                     @RequestParam String path) {
        return ResponseEntity.ok(microserviceService.registerNew(name, url, path));
    }

    private String getRedirectUrl(Microservice microservice, String path) {
        String microserviceUrl = microservice.getUrl();
        if (microserviceUrl.endsWith("/")) {
            microserviceUrl = microserviceUrl.substring(0, microserviceUrl.length() - 1);
        }
        return microserviceUrl + path;
    }
}

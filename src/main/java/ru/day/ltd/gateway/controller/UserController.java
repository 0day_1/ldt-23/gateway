package ru.day.ltd.gateway.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.day.ltd.gateway.data.dto.UserRegistrationDTO;
import ru.day.ltd.gateway.data.enums.UserType;
import ru.day.ltd.gateway.service.ApiUserService;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("auth/api/v1/user")
public class UserController {

    private final ApiUserService apiUserService;

    public UserController(ApiUserService apiUserService) {
        this.apiUserService = apiUserService;
    }

    @GetMapping("")
    public ResponseEntity<?> getUser(@RequestParam(required = false) UUID userId) {
        if (userId != null) {
            try {
                return ResponseEntity.ok(apiUserService.getById(userId));
            } catch (IllegalArgumentException e) {
                log.error("Can't get user", e);
                return ResponseEntity.badRequest().body(e.getMessage());
            }
        } else {
            return ResponseEntity.ok(apiUserService.getCurrentUser());
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(apiUserService.getAll());
    }

    @PostMapping("/new")
    public ResponseEntity<?> createNew(@RequestBody UserRegistrationDTO userRegistrationDTO) {
        String rawType = userRegistrationDTO.getUserType();
        UserType userType;
        try {
            userType = UserType.fromString(rawType);
        } catch (IllegalArgumentException iae) {
            return ResponseEntity.badRequest().body("No such user type");
        }
        return ResponseEntity.ok(apiUserService.register(userRegistrationDTO.getUsername(),
                userRegistrationDTO.getPassword(),
                userType));
    }

    @GetMapping("/usernamecheck")
    public ResponseEntity<Boolean> usernameCheck(@RequestParam String username) {
        return ResponseEntity.ok(apiUserService.checkUsernameIsFree(username));
    }

}

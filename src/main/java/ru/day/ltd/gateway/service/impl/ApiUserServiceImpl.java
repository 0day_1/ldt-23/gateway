package ru.day.ltd.gateway.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.day.ltd.gateway.data.dto.UserDTO;
import ru.day.ltd.gateway.data.entity.ApiUser;
import ru.day.ltd.gateway.data.enums.UserType;
import ru.day.ltd.gateway.data.repo.ApiUserRepository;
import ru.day.ltd.gateway.security.ApiUserDetails;
import ru.day.ltd.gateway.service.ApiUserService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service(ApiUserService.NAME)
public class ApiUserServiceImpl implements ApiUserService {

    private final ApiUserRepository apiUserRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApiUserServiceImpl(ApiUserRepository apiUserRepository, PasswordEncoder passwordEncoder) {
        this.apiUserRepository = apiUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<UserDTO> getAll() {
        return apiUserRepository.findAll().stream()
                .map(this::toUserDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getById(UUID userId) throws IllegalArgumentException {
        if (userId == null) {
            throw new IllegalArgumentException("User id is null");
        }
        return apiUserRepository.findById(userId)
                .map(this::toUserDTO)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
    }

    @Override
    public UserDTO getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        ApiUserDetails apiUserDetails = (ApiUserDetails) authentication.getPrincipal();
        ApiUser apiUser = apiUserDetails.getApiUser();
        return toUserDTO(apiUser);
    }

    @Override
    public boolean checkUsernameIsFree(String username) {
        return apiUserRepository.findByUsername(username).isEmpty();
    }

    @Override
    @Transactional
    public UserDTO register(String username, String password, UserType userType) {
        if (userType.equals(UserType.ADMIN)) {
            return registerAdmin(username, password);
        }
        ApiUser apiUser = new ApiUser();
        apiUser.setUsername(username);
        apiUser.setPassword(passwordEncoder.encode(password));
        apiUser.setUserType(userType);
        return toUserDTO(apiUserRepository.save(apiUser));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    private UserDTO registerAdmin(String username, String password) {
        ApiUser apiUser = new ApiUser();
        apiUser.setUsername(username);
        apiUser.setPassword(passwordEncoder.encode(password));
        apiUser.setUserType(UserType.ADMIN);
        return toUserDTO(apiUserRepository.save(apiUser));
    }

    private UserDTO toUserDTO(ApiUser apiUser) {
        return new UserDTO(apiUser.getId(),
                apiUser.getUsername(),
                apiUser.getUserType().getValue());
    }

}

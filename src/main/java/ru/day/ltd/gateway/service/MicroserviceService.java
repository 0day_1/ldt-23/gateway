package ru.day.ltd.gateway.service;

import ru.day.ltd.gateway.data.entity.Microservice;

public interface MicroserviceService {
    String NAME = "microserviceService";

    Microservice getByPath(String path) throws IllegalArgumentException;

    Microservice registerNew(String name, String url, String path);
}

package ru.day.ltd.gateway.service;

import ru.day.ltd.gateway.data.dto.UserDTO;
import ru.day.ltd.gateway.data.enums.UserType;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

public interface ApiUserService {
    String NAME = "apiUserService";

    List<UserDTO> getAll();

    /**
     * @param userId UUID of user
     * @return UserDTO with userId and username and userType
     * @throws IllegalArgumentException if user with such id not found or userId is null
     */
    UserDTO getById(UUID userId) throws IllegalArgumentException;

    /**
     * @return UserDTO with userId and username and userType
     */
    UserDTO getCurrentUser();

    /**
     * @param username username to check
     * @return true if no user with this username
     */
    boolean checkUsernameIsFree(String username);

    @Transactional
    UserDTO register(String username, String password, UserType userType);
}

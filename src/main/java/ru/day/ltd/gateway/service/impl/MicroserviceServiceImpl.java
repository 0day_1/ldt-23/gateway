package ru.day.ltd.gateway.service.impl;

import org.springframework.stereotype.Service;
import ru.day.ltd.gateway.data.entity.Microservice;
import ru.day.ltd.gateway.data.repo.MicroserviceRepository;
import ru.day.ltd.gateway.service.MicroserviceService;

@Service(MicroserviceService.NAME)
public class MicroserviceServiceImpl implements MicroserviceService {

    private final MicroserviceRepository microserviceRepository;

    public MicroserviceServiceImpl(MicroserviceRepository microserviceRepository) {
        this.microserviceRepository = microserviceRepository;
    }

    @Override
    public Microservice getByPath(String path) throws IllegalArgumentException {
        return microserviceRepository.findByPath(path).orElseThrow(() -> new IllegalArgumentException("Microservice not found"));
    }

    @Override
    public Microservice registerNew(String name, String url, String path) {
        // TODO validation
        Microservice microservice = new Microservice();
        microservice.setName(name);
        microservice.setUrl(url);
        microservice.setPath(path);
        return microserviceRepository.save(microservice);
    }

}

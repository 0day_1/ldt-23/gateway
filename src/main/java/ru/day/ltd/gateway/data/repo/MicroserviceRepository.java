package ru.day.ltd.gateway.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.day.ltd.gateway.data.entity.Microservice;

import java.util.Optional;
import java.util.UUID;

public interface MicroserviceRepository extends JpaRepository<Microservice, UUID> {

    Optional<Microservice> findByPath(String path);

}

package ru.day.ltd.gateway.data.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserRegistrationDTO implements Serializable {
    private static final long serialVersionUID = 5895693525686350698L;

    private String username;
    private String password;
    private String userType;
}

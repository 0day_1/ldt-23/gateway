package ru.day.ltd.gateway.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.day.ltd.gateway.data.entity.ApiUser;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ApiUserRepository extends JpaRepository<ApiUser, UUID> {

    Optional<ApiUser> findByUsername(String username);

}

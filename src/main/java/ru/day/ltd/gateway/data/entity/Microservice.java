package ru.day.ltd.gateway.data.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "MICROSERVICE")
@Getter @Setter
public class Microservice {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    @NonNull
    private UUID id;

    @NonNull
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @NonNull
    @Column(name = "URL", nullable = false)
    private String url;

    @NonNull
    @Column(name = "PATH", nullable = false, unique = true)
    private String path;

}

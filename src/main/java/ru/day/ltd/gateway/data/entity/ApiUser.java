package ru.day.ltd.gateway.data.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import ru.day.ltd.gateway.data.enums.UserType;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "API_USER")
@Getter @Setter
public class ApiUser {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    @NonNull
    private UUID id;

    @NonNull
    @Column(name = "USERNAME", nullable = false)
    private String username;

    @NonNull
    @Column(name = "password", nullable = false)
    private String password;

    @NonNull
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Column(name = "TYPE", nullable = false)
    private String type;

    public UserType getUserType() {
        return UserType.fromString(type);
    }

    public void setUserType(UserType userType) {
        this.type = Objects.requireNonNull(userType).getValue();
    }

}

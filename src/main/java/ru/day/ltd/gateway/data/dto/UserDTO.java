package ru.day.ltd.gateway.data.dto;

import lombok.*;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserDTO implements Serializable {
    private static final long serialVersionUID = -4573260990636417037L;

    private UUID id;
    private String username;
    private String type;

}

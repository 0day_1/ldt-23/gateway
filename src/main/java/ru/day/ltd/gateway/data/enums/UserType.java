package ru.day.ltd.gateway.data.enums;

public enum UserType {

    ADMIN("ADMIN"),
    MODERATOR("MODERATOR"),
    LANDLORD("LANDLORD"),
    RENTER("RENTER");

    private final String value;

    UserType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getAuthorityString() {
        return "ROLE_" + value;
    }

    public static UserType fromString(String value) throws IllegalArgumentException {
        for (UserType userType : UserType.values()) {
            if (userType.getValue().equals(value)) {
                return userType;
            }
        }
        throw new IllegalArgumentException("No such value in UserType");
    }

}
